"""
In this arrangement, the framework selects views/recharge/index.html
"""
def index_inline_content_in_a_view():
    response.title="Recharge with Air"
    response.subtitle="A range of plans only for you!"
    return dict()

"""
Here we construct a FORM object and embed it within the view
"""
def index_embed_in_a_view():
    response.title="Online prepaid recharge with Air"
    response.subtitle="A range of plans only for you!"
    form = FORM(TAG('<phone-number>Your phone number:</phone-number>'), \
                INPUT(_name='phone-number'), \
                INPUT(_type='submit', _class='input-button'))
    return dict(prepaid_recharge_form=form)

"""
Here we construct a FORM object and embed it within the view.
Also enable validation of the phone number
"""
def index():
    response.title="Online prepaid recharge with Air"
    response.subtitle="A range of plans only for you!"
    
    form = FORM(TAG('<phone-number>Your phone number:</phone-number>'), \
                INPUT(_name='phone-number', \
                      requires=IS_MATCH('\d{10}', error_message='please enter a valid 10 digit number')), \
                 makePlans(), INPUT(_type='submit', _class='input-button'))
    if form.accepts(request, session):
        response.status_msg="The phone number is accepted"
    elif form.errors:
        response.status_msg="Number was invalid. Please reenter the number"
    else:
        response.status_msg="Please fill the form"

    return dict(prepaid_recharge_form=form)


def makePlans():
    plans = DIV(_class='plans')
    for plan in response.rechargePlans:
        plans.append(INPUT(_type='radio', _value=plan, _name='aPlan', _class='plan-radio'))
        plans.append(SPAN(plan, _class='plan-name'))
    return plans













